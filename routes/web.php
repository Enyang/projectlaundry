<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@home');
Route::get('/about', 'PagesController@about');
Route::get('/contact', 'PagesController@contact');


Route::get('/costumers', 'CostumersController@index');
Route::get('/costumers/create', 'CostumersController@create');
Route::get('/costumers/{costumer}', 'CostumersController@show');
Route::post('/costumers', 'CostumersController@store');
Route::delete('/costumers/{costumer}', 'CostumersController@destroy');

Route::get('/beritas', 'BeritasController@index');
Route::get('/beritas/create', 'BeritasController@create');
Route::get('/beritas/{berita}', 'BeritasController@show');
Route::post('/beritas', 'BeritasController@store');
Route::delete('/beritas/{berita}', 'BeritasController@destroy');
