<!DOCTYPE html>
<html lang="en">
<head>
  <title>@yield('title')</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Sneaky</title>
  <link rel="icon" href="/img/Fevicon.png" type="image/png">

  <link rel="stylesheet" href="/vendors/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="/vendors/themify-icons/themify-icons.css">
  <link rel="stylesheet" href="/vendors/owl-carousel/owl.theme.default.min.css">
  <link rel="stylesheet" href="/vendors/owl-carousel/owl.carousel.min.css">
  <link rel="stylesheet" href="/vendors/Magnific-Popup/magnific-popup.css">

  <link rel="stylesheet" href="/css/style.css">
</head>
<body>

  <!--================ Header Menu Area start =================-->
  <header class="header_area">
    <div class="main_menu">
      <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container box_1620">
          <a class="navbar-brand logo_h" href="index.html"><img src="img/logo.png" alt=""></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

          <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
            <ul class="nav navbar-nav menu_nav justify-content-end">
              <li class="nav-item"><a class="nav-link" href="{{ url('/') }}">Beranda</a></li> 
              <li class="nav-item"><a class="nav-link" href="{{ url('/about') }}">Tentang Kami</a></li> 
              <!-- <li class="nav-item"><a class="nav-link" href="{{ url('/costumers') }}">Pelanggan</a>
 -->
              <li class="nav-item"><a class="nav-link" href="{{ url('/beritas') }}">Berita</a>
              <li class="nav-item"><a class="nav-link" href="{{ url('/contact') }}">Kontak Kami</a>

             <!--  <li class="nav-item submenu dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                  aria-expanded="false">Blog</a>
                <ul class="dropdown-menu">
                  <li class="nav-item"><a class="nav-link" href="blog.html">Blog Single</a></li>
                  <li class="nav-item"><a class="nav-link" href="blog-details.html">Blog Details</a></li>
                </ul>
              </li> -->
            </ul>
          </div> 
        </div>
      </nav>
    </div>
  </header>
  <!--================Header Menu Area =================-->
  
  @yield('container')
  
  <!-- ================ start footer Area ================= -->
  <footer class="footer-area section-gap">
    <div class="container">
      <div class="row">
        <div class="col-xl-2 col-sm-6 mb-4 mb-xl-0 single-footer-widget">
          <h4>Produk Kami</h4>
          <ul>
            <li><a href="#">Cuci dan Setrika</a></li>
            <li><a href="#">Cuci Kering</a></li>
            <li><a href="#">Cuci Kiloan</a></li>
            <li><a href="#">Cuci Satuan</a></li>
          </ul>
        </div>
        <div class="col-xl-2 col-sm-6 mb-4 mb-xl-0 single-footer-widget">
          <h4>Fitur</h4>
          <ul>
            <li><a href="#">Antar Jemput Laundry</a></li>
            <li><a href="#">Paket Express</a></li>
          </ul>
        </div>
        <div class="col-xl-2 col-sm-6 mb-4 mb-xl-0 single-footer-widget">
          <h4>Sumber Daya</h4>
          <ul>
            <li><a href="#">Mesin Terbaru</a></li>
            <li><a href="#">Detergen Terbaik</a></li>
            <li><a href="#">Parfum Grade A</a></li>
          </ul>
        </div>
        <div class="col-xl-4 col-md-8 mb-4 mb-xl-0 single-footer-widget">
          <h4>Kontak</h4>
          <p><a href="https://mail.google.com/mail/u/0/#inbox">enyangindram@gmail.com</a></p>
        </div>
      </div>
      <div class="footer-bottom row align-items-center text-center text-lg-left">
        <p class="footer-text m-0 col-lg-8 col-md-12"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="ti-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
        <div class="col-lg-4 col-md-12 text-center text-lg-right footer-social">
          <a href="#"><i class="ti-facebook"></i></a>
          <a href="#"><i class="ti-twitter-alt"></i></a>
        </div>
      </div>
    </div>
  </footer>
  <!-- ================ End footer Area ================= -->

  <script src="vendors/jquery/jquery-3.2.1.min.js"></script>
  <script src="vendors/bootstrap/bootstrap.bundle.min.js"></script>
  <script src="vendors/owl-carousel/owl.carousel.min.js"></script>
  <script src="vendors/nice-select/jquery.nice-select.min.js"></script>
  <script src="vendors/Magnific-Popup/jquery.magnific-popup.min.js"></script>
  <script src="js/jquery.ajaxchimp.min.js"></script>
  <script src="js/mail-script.js"></script>
  <script src="js/main.js"></script>
</body>
</html>