@extends('layout/main')

@section('title', 'Tambah data Pelanggan')

@section('container')
<div class="container">
    <div class="row">
       <div class="col-6">
           <h1 class="mt-3" >Input Pelanggan</h1>

           <form method="post" action="/costumers">
           @csrf
             <div class="form-group">
                <label for="id_costumer">Id_Pelanggan</label>
                <input type="text" class="form-control @error('id_costumer') is-invalid @enderror" id="id_costumer"  name="id_costumer" value="{{ old('id_costumer') }}">
                @error('id_costumer')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
             </div>
             <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" value="{{ old('nama') }}">
                 @error('nama')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
             </div>
             <div class="form-group">
                <label for="alamat">Alamat</label>
                <input type="text" class="form-control" id="alamat" name="alamat" value="{{ old('alamat') }}">
             </div>
             <div class="form-group">
                <label for="nohp">Nohp</label>
                <input type="text" class="form-control" id="nohp" name="nohp" value="{{ old('email') }}">
             </div>
             <button type="submit" class="btn btn-primary">Tambah Data</button>
            </form>

    
       </div>
    </div>
 </div>
 @endsection