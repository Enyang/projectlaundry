@extends('layout/main')

@section('title', 'Daftar Pelanggan')

@section('container')
<div class="container">
    <div class="row">
       <div class="col-10">
           <h2 class="mt-3" >Daftar Pelanggan</h2>
           <a href="/costumers/create" class="btn btn-primary my-2">Tambah data</a>

           

            @if (session('status'))
               <div class="alert alert-success">
                  {{ session('status') }}
               </div>
            @endif


           <table class="table">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Id_Costumer</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Aksi</th>
                  </tr>
                </thead>

                <tbody>
                @foreach( $costumers as $costumer)
                <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ $costumer->id }}</td>
                    <td>{{ $costumer->nama }}</td>
                    <td>
                    <a href="/costumers/{{ $costumer->id }}" class="badge badge-info">detail</a>
                    </td>
                </tr>
                @endforeach
                </tbody>
           </table>
       </div>
    </div>
 </div>
 @endsection