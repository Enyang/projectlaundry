@extends('layout/main')
@section('title', 'Daftar Pelanggan')

@section('container')
<div class="container">
    <div class="row">
       <div class="col-7">
          <h1 class="mt-3" >Detail Pelanggan</h1>
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">{{ $costumer->id_costumer }}</h5>
              <h6 class="card-subtitle mb-2 text-muted">{{ $costumer->nama }}</h6>
              <h6 class="card-subtitle mb-2 text-muted">{{ $costumer->alamat }}</h6>
              <h6 class="card-subtitle mb-2 text-muted">{{ $costumer->nohp }}</h6>

              <button type="submit" class="btn btn-primary">Edit</button>
              <form action="{{ $costumer->id }}" method ="post" class="d-inline"> 
                 @method('delete')
                 @csrf
              <button type="submit" class="btn btn-danger">Delete</button></form>
              <a href="/costumers" class="card-link">Back</a>
            </div>
          </div>
       </div>
    </div>
</div>
@endsection