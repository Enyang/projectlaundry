@extends('layout/main')

@section('title', 'Berita')

@section('container')

  <!--================Hero Banner Section start =================-->
  <section class="hero-banner hero-banner-sm">
    <div class="hero-wrapper">
      <div class="hero-left">
        <h1 class="hero-title">Berita Terkini</h1>
        <p>Berikut ini berita-berita terkini <br class="d-none d-xl-block"> mengenai laundry </p>
        <ul class="hero-info d-none d-lg-block">
          <li>
            <img src="img/banner/fas-service-icon.png" alt="">
            <h4>Layanan Cepat</h4>
          </li>
          <li>
            <img src="img/banner/botol.png" alt="">
            <h4>Wangi</h4>
          </li>
          <li>
            <img src="img/banner/washing-machine.png" alt="">
            <h4>1 Orang 1 Mesin</h4>
          </li>
        </ul>
      </div>
      <div class="hero-right">
        <div class="owl-carousel owl-theme w-100 hero-carousel">
          <div class="hero-carousel-item">
            <img class="img-fluid" src="img/banner/foldedq.jpg" alt="">
          </div>
        </div>
      </div>
      <ul class="social-icons d-none d-lg-block">
        <li><a href="https://web.facebook.com/enyangindra?ref=bookmarks"><i class="ti-facebook"></i></a></li>
        <li><a href="#"><i class="ti-twitter"></i></a></li>
        <li><a href="#"><i class="ti-instagram"></i></a></li>
      </ul>
    </div>
  </section>
  <!--================Hero Banner Section end =================-->

  <!--================Blog Area =================-->
  <section class="blog_area section-margin">
      <div class="container">
          <div class="row">
              <div class="col-lg-8 mb-5 mb-lg-0">
                  <div class="blog_left_sidebar">
                    @foreach( $beritas as $berita)
                      <article class="blog_item">
                        <div class="blog_item_img">
                          <img class="card-img rounded-0" src="img/blog/main-blog/3257118.jpg" alt="">
                        </div>
                        
                        <div class="blog_details">
                                <h2>{{ $berita->judul }}</h2>
                            
                            <p>{{ $berita->isi }}</p>
                            <ul class="blog-info-link">
                              <li><a href="#"><i class="ti-calendar"></i>{{ $berita->created_at }}</a></li>
                            
                        
                        <form action="/beritas/{{ $berita->id }}" method ="post" class="d-inline"> 
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                        </ul>
                        </div>
                      </article>
                      @endforeach

                      <nav class="blog-pagination justify-content-center d-flex">
                          <ul class="pagination">
                              <li class="page-item">
                                  <a href="#" class="page-link" aria-label="Previous">
                                      <span aria-hidden="true">
                                          <span class="ti-arrow-left"></span>
                                      </span>
                                  </a>
                              </li>
                              <li class="page-item">
                                  <a href="#" class="page-link">1</a>
                              </li>
                              <li class="page-item active">
                                  <a href="#" class="page-link">2</a>
                              </li>
                              <li class="page-item">
                                  <a href="#" class="page-link" aria-label="Next">
                                      <span aria-hidden="true">
                                          <span class="ti-arrow-right"></span>
                                      </span>
                                  </a>
                              </li>
                          </ul>
                      </nav>
                  </div>
              </div>
              <div class="col-lg-4">
                  <div class="blog_right_sidebar">
                      <aside class="single_sidebar_widget search_widget">
                            <a href="/beritas/create" class="button rounded-1 primary-bg text-white w-100">Tambah Berita</a>
                          
                      </aside>

                      <aside class="single_sidebar_widget post_category_widget">
                        <h4 class="widget_title">Kategori</h4>
                        <ul class="list cat-list">
                            <li>
                                <a href="#" class="d-flex">
                                    <p>Mencuci</p>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="d-flex">
                                    <p>Detergent</p>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="d-flex">
                                    <p>Teknologi Terbaru</p>
                                </a>
                            </li>
                        </ul>
                      </aside>
           
                  </div>
              </div>
          </div>
      </div>
  </section>
  <!--================Blog Area =================-->
 @endsection