@extends('layout/main')

@section('title', 'Tambah data')

@section('container')


<div class="container">
    <div class="row">
       <div class="col-6">
           <h1 class="mt-3" >Input Berita</h1>

           <form method="post" action="/beritas" class="form-contact contact_form">
           @csrf
            <div class="col-12">
             <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text" class="form-control @error('judul') is-invalid @enderror" id="judul" name="judul" value="{{ old('judul') }}">
                 @error('judul')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
             </div>
           </div>
             <div class="col-12">
             <div class="form-group">
                <label for="isi">Content</label>
                <textarea type="text" class="form-control w-100" cols="30" rows="9" id="isi" name="isi" value="{{ old('isi') }}"></textarea>
             </div>
           </div>
             <button type="submit" class="btn btn-primary">Tambah Data</button>
            </form>
       </div>
    </div>
 </div>
 @endsection