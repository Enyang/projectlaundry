@extends('layout/main')

@section('title', 'Tentang Sneaky')

@section('container')
  <!--================Hero Banner Section start =================-->
  <section class="hero-banner hero-banner-sm">
    <div class="hero-wrapper">
      <div class="hero-left">
        <h1 class="hero-title">Tentang Kami </h1>
        <p>Kami menawarkan jasa layanan cuci dan seterika berdasarkan jumlah berat (Kg) cucian konsumen, sehingga harga yang ditawarkan lebih murah dari jasa layanan laundry yang sudah ada, namun tetap mengutamakan kualitas.<br class="d-none d-xl-block"></p>
        <ul class="hero-info d-none d-lg-block">
          <li>
            <img src="img/banner/fas-service-icon.png" alt="">
            <h4>Layanan Cepat</h4>
          </li>
          <li>
            <img src="img/banner/botol.png" alt="">
            <h4>Wangi</h4>
          </li>
          <li>
            <img src="img/banner/washing-machine.png" alt="">
            <h4>1 Orang 1 Mesin</h4>
          </li>
        </ul>
      </div>
      <div class="hero-right">
        <div class="owl-carousel owl-theme w-100 hero-carousel">
          <div class="hero-carousel-item">
            <img class="img-fluid" src="img/banner/foldedq.jpg" alt="">
          </div>
        </div>
      </div>
      <ul class="social-icons d-none d-lg-block">
        <li><a href="https://web.facebook.com/enyangindra?ref=bookmarks"><i class="ti-facebook"></i></a></li>
        <li><a href="https://twitter.com/home"><i class="ti-twitter"></i></a></li>
        <li><a href="https://www.instagram.com/n_nyangg/"><i class="ti-instagram"></i></a></li>
      </ul>
    </div>
    <br>
  </section>
  <!--================Hero Banner Section end =================-->

  <section class="about section-margin pb-xl-70">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-xl-6 mb-5 mb-md-0 pb-5 pb-md-0">
          <div class="img-styleBox">
            <div class="styleBox-border">
              <img class="styleBox-img1 img-fluid" src="img/home/2642431.jpg" alt="">
            </div>
            <img class="styleBox-img2 img-fluid" src="img/home/3257118.jpg" alt="">
          </div>
        </div>
        <div class="col-md-6 pl-md-5 pl-xl-0 offset-xl-1 col-xl-5">
          <div class="section-intro mb-lg-4">
            <h4 class="intro-title">Tentang Kami</h4>
            <h2>Kami mengutamakan kepuasan pelanggan</h2>
          </div>
          <p>
            SneakyLaundry adalah Jasa laundry satuan atau dry cleaning dan kiloan kami melayani jasa cuci / laundry semua kalangan dari rumah, kontrakan, kos, perumahan besar dan kecil bahkan sampai apartement dan hotel.
          </p> 
          <p>
            Perlengakapan laundry kami mengunakan semua perlengkapan pabrikan atau merk yang sudah banyak dikenal dengan hasil laundry yang maksimal, dengan pengharum grade A dan menggunakan setrika uap, laundry kami dikerjakan dengan tenaga ahli berpengalaman semua terlatih dan dituntut untuk bersih dan rapi.
          </p>
          <p>
            SneakyLaundy mengutamakan kepuasan pelanggan dan kebersihan setiap pekerjaan cucian / laundryan, komitment mengerjakan pekerjaan dengan cepat, tepat waktu dan tanpa menunggu lama maka dari itu kami mengunakan mesin satu customer satu mesin agar tetap higienis dan hasil yang lebih cepat.
          </p>
        </div>
      </div>
    </div>
  </section>
 @endsection