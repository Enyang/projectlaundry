 @extends('layout/main')

 @section('title', 'Beranda LaundrySneaky')

 @section('container')
 <!--================Hero Banner Section start =================-->
  <section class="hero-banner">
    <div class="hero-wrapper">
      <div class="hero-left">
        <h1 class="hero-title">Sahabat sejati dalam mencuci</h1>
        <div class="d-sm-flex flex-wrap">
          <a class="hero-banner__video" href="https://www.youtube.com/watch?v=1r8fA8kQ1Ik">Lihat Video</a>          
        </div>
        <ul class="hero-info d-none d-lg-block">
          <li>
            <img src="img/banner/fas-service-icon.png" alt="">
            <h4>Antar Jemput</h4>
          </li>
          <li>
            <img src="img/banner/washing-machine.png" alt="">
            <h4>1 Mesin 1 Orang</h4>
          </li>
        </ul>
      </div>
      <div class="hero-right">
        <div class="owl-carousel owl-theme hero-carousel">
          <div class="hero-carousel-item">
            <img class="img-fluid" src="img/banner/3257203.jpg" alt="">
          </div>
          <div class="hero-carousel-item">
            <img class="img-fluid" src="img/banner/setrika.jpg" alt="">
          </div>
          <div class="hero-carousel-item">
            <img class="img-fluid" src="img/banner/2714933.jpg" alt="">
          </div>
          <div class="hero-carousel-item">
            <img class="img-fluid" src="img/banner/laundromat4.jpg" alt="">
          </div>
        </div>
      </div>
      <ul class="social-icons d-none d-lg-block">
        <li><a href="https://web.facebook.com/enyangindra?ref=bookmarks"><i class="ti-facebook"></i></a></li>
        <li><a href="https://twitter.com/home"><i class="ti-twitter"></i></a></li>
        <li><a href="https://www.instagram.com/n_nyangg/"><i class="ti-instagram"></i></a></li>
      </ul>
    </div>
  </section>
  <!--================Featured Section Start =================-->
  <section class="section-margin mb-lg-100">
    <div class="container">
      <div class="section-intro mb-75px">
        <h4 class="intro-title">Layanan Kami</h4>
        <h2>Jasa yang kami tawarkan</h2>
      </div>


      <div class="owl-carousel owl-theme featured-carousel">
        <div class="featured-item">
          <img class="card-img rounded-0" src="img/home/laundry1.jpg" alt="">
          <div class="item-body">
            <a href="#">
              <h3>Layanan Kilat</h3>
            </a>
            <p>Hasil laundry-an akan terselesaikan dalam waktu 6 jam kemudian</p>
            <div class="d-flex justify-content-between">
              <ul class="rating-star">
                <li><i class="ti-star"></i></li>
                <li><i class="ti-star"></i></li>
                <li><i class="ti-star"></i></li>
                <li><i class="ti-star"></i></li>
                <li><i class="ti-star"></i></li>
              </ul>
              <h3 class="price-tag">Rp.20.000</h3>
            </div>
          </div>
        </div>
        <div class="featured-item">
          <img class="card-img rounded-0" src="img/home/laundry2.jpg" alt="">
          <div class="item-body">
            <a href="#">
              <h3>Layanan Express</h3>
            </a>
            <p>Hasil laundry-an akan terselesaikan dalam waktu 1 hari.</p>
            <div class="d-flex justify-content-between">
              <ul class="rating-star">
                <li><i class="ti-star"></i></li>
                <li><i class="ti-star"></i></li>
                <li><i class="ti-star"></i></li>
                <li><i class="ti-star"></i></li>
                <li><i class="ti-star"></i></li>
              </ul>
              <h3 class="price-tag">Rp.13.000</h3>
            </div>
          </div>
        </div>
        <div class="featured-item">
          <img class="card-img rounded-0" src="img/home/laundry3.jpg" alt="">
          <div class="item-body">
            <a href="#">
              <h3>Layanan Biasa</h3>
            </a>
            <p>Hasil laundry-an akan terselesaikan dalam waktu 2 sampai 3 hari kemudian.</p>
            <div class="d-flex justify-content-between">
              <ul class="rating-star">
                <li><i class="ti-star"></i></li>
                <li><i class="ti-star"></i></li>
                <li><i class="ti-star"></i></li>
                <li><i class="ti-star"></i></li>
                <li><i class="ti-star"></i></li>
              </ul>
              <h3 class="price-tag">Rp.8.000</h3>
            </div>
          </div>
        </div>
        <div class="featured-item">
          <img class="card-img rounded-0" src="img/home/laundry4.jpg" alt="">
          <div class="item-body">
            <a href="#">
              <h3>Layanan Cuci Satuan</h3>
            </a>
            <p>Layanan ini menawarkan mencuci pakaian khusus seperti jas, sepatu, dll</p>
            <div class="d-flex justify-content-between">
              <ul class="rating-star">
                <li><i class="ti-star"></i></li>
                <li><i class="ti-star"></i></li>
                <li><i class="ti-star"></i></li>
                <li><i class="ti-star"></i></li>
                <li><i class="ti-star"></i></li>
              </ul>
              <h3 class="price-tag">Tergantung jenis</h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--================Featured Section End =================-->

  @endsection