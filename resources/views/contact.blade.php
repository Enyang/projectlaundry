@extends('layout/main')

@section('title', 'Contact')

@section('container')

<!-- ================ contact section start ================= -->
  <section class="section-margin">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="contact-title">Kontak Kami</h2>
        </div>
        <div class="col-lg-12">
          <form class="form-contact contact_form" action="contact_process.php" method="post" id="contactForm" novalidate="novalidate">
            <div class="row">
              <div class="col-12">
                
          <center>
         <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.637801265475!2d107.60014531408001!3d-6.9338209949902465!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e8994a7cfd2b%3A0x3d6e45e721b25b16!2slaundry88!5e0!3m2!1sen!2sid!4v1585141429939!5m2!1sen!2sid" width="900" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
         <br>
         <br>
          <div class="row">
          <div class="col-md-4">
            <img src="/img/gallery/gmail.png" style="height:100px; width:100px">
            <br>
            <br>
            <a href="mailto:Aku@gmail.com">as@gmail.com</a>
          </div>

            <div class="col-md-4">
            <img src="/img/gallery/location.png" style="height:100px; width:100px">
            <br>
            <br>
            Jl.jakarta no 2131
           </div>

            <div class="col-md-4">
            <img src="/img/gallery/smartphone.png" style="height:100px; width:100px">
            <br>
            <br>
             <a href="tel:087824324189">087824324189</a>
           
          </div>
           </div>
         </center>
        </div>

        </div>
    </div>
  </section>
	<!-- ================ contact section end ================= -->
@endsection